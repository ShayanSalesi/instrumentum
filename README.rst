Introduction
==========================

The goal of instrumentum is to provide a suite of highly customizable toy
financial instruments that can be used in experimentation and testing.

Credits
-------

- `Distribute`_
- `Buildout`_
- `modern-package-template`_

.. _Buildout: http://www.buildout.org/
.. _Distribute: http://pypi.python.org/pypi/distribute
.. _`modern-package-template`: http://pypi.python.org/pypi/modern-package-template
