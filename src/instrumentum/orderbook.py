from __future__ import annotations

import pandas as pd
import numpy as np
import random
from typing import Union
import time


class OrderBook:
    """
    An orderbook for a security.

    This is an abstract base class. Only subclasses should be instantiated.

    Attributes
    ----------
    symbol: str
        The trading symbol of this security.
    id_: int
        ID number of this security.
    type_: str
        Security type, can be one of
            "STK": stock
            "FUT": future
            "CRR": currency pair
            "OPT": option
    """
    symbol: str
    id: int
    type: str

    def __init__(self, symbol: str, id_: int, type_: str) -> None:
        self.symbol = symbol
        self.id = id_
        self.type = type_

    def generate_session(self, num_trades=100, verbose=True, lag=1.0) -> None:
        """
        Generate a trading session in this order book with <num_trades>
        trades.
        """
        raise NotImplementedError


class RandomStockOrderBook(OrderBook):
    """
    A randomly generated order book for a stock.

    Attributes
    ----------
    active: bool
        If true, gives an order book with high activity and liquidity.
    min_price: int
        The minimum price that the initial quote can have.
    symbol: str
        The trading symbol of this security.
    id_: int
        ID number of this security.
    type_: str
        Security type, can be one of
            "STK": stock
            "FUT": future
            "CRR": currency pair
            "OPT": option

    Methods
    -------
    generate_session(num_trades=100, verbose=True)
        Generates a trading session on this order book, with <num_trades>
        trades.
    """
    # Public attributes
    active: bool
    best_bid: float
    best_ask: float
    order_book: pd.DataFrame
    # Private attributes
    _max_price: float
    _min_price: float

    def __init__(
        self,
        active=True,
        min_price=40.0,
        symbol="FOO",
        id_=1,
        type_="STK"
    ) -> None:
        """Initialize an order book."""
        OrderBook.__init__(self, symbol, id_, type_)
        self.active = True
        if min_price > 100:
            print("minimum price must not exceed 99")
            min_price = 50
        self.best_bid = round(random.uniform(min_price, 99), 2)
        self.best_ask = self.best_bid + 0.01 # Assuming closed spread
        self._max_price = self.best_ask + 0.49
        self._min_price = self.best_bid - 0.49
        self.order_book = self._make_book()

    def _make_book(self) -> pd.DataFrame:
        """Makes an initial order book."""
        behind_mkt_ask = np.random.normal(loc=100, scale=10, size=40).astype(int)
        visible_book_ask = np.random.normal(loc=300, scale=50, size=10).astype(int)
        visible_book_bid = np.random.normal(loc=300, scale=50, size=10).astype(int)
        behind_mkt_bid = np.random.normal(loc=100, scale=10, size=40).astype(int)
        bid_sizes = np.append(visible_book_bid, behind_mkt_bid)
        ask_sizes = np.append(behind_mkt_ask, visible_book_ask)
        order_book = pd.DataFrame(
            data={
            "bid size": np.append(np.zeros(50), bid_sizes),
            "prices": np.flip(np.linspace(self._min_price, self._max_price, 100)),
            "ask size": np.append(ask_sizes, np.zeros(50))
                }
            )
        order_book["NBBO"] = self._set_nbbo_col(order_book)

        return order_book

    def _is_nbbo_(self, price: float) -> str:
        """
        Checks if <price> is the NBBO.

        Returns a string:
            - "bid" ----> <price> is best bid
            - "ask" ----> <price> is best ask
            - "neither" ----> <price> is behind the market
        """
        if price == self.best_bid:
            return "bid"
        elif price == self.best_ask:
            return "ask"
        else:
            return "neither"

    def _set_nbbo_col(self, df: pd.DataFrame):
        """Setter for NBBO col"""
        return df["prices"].map(lambda x: self._is_nbbo_(x))

    def generate_session(self, num_trades=100, verbose=True, lag=1.0) -> None:
        """
        Generate a trading session on the order book.

        Parameters
        ----------
        num_trades: int
            the number of trades that are executed in this session. Order
            cancellations are counted in this. Default is 100.
        verbose: bool
            If true, prints the order book as it is being traded.
        lag: float
            lag in seconds between order executions.
        """
        trades_executed = 0
        print("trades executed: ", trades_executed)
        if verbose:
            print(self.order_book.to_string())
        while trades_executed < num_trades:
            time.sleep(lag)
            pending_order = self._generate_order()
            if self._process_order(pending_order):
                trades_executed += 1
            print("trades executed: ", trades_executed)
            if verbose:
                print(self.order_book.to_string())

    def _process_order(self, order: tuple) -> bool:
        """
        Applies an order to the standing order book.
        """
        ord_type, ord_side, ord_size, ord_price = order
        if ord_type == "LMT" and ord_side == "BUY":
            self.order_book.loc[self.order_book["prices"] == ord_price, "bid size"] += ord_size
        elif ord_type == "LMT" and ord_side == "SELL":
            self.order_book.loc[self.order_book["prices"] == ord_price, "ask size"] += ord_size
        # In this case the order is for market, so apply the size recursively
        elif ord_type == "MKT" and ord_side == "BUY":
            self._apply_mkt_buy(ord_size)
        else:
            self._apply_mkt_sell(ord_size)
        return True

    def _apply_mkt_buy(self, ord_size: int) -> None:
        """A recursive function that applies a market buy order."""
        # Base case: order size is fine to be processed
        print(self.order_book.loc[self.order_book["NBBO"] == "ask"]["ask size"].values)
        if self.order_book.loc[self.order_book["NBBO"] == "ask"]["ask size"].values >= ord_size:
            self.order_book.loc[self.order_book["NBBO"] == "ask", "ask size"] -= ord_size
        else:
            self._move_nbbo("up")
            self._apply_mkt_buy(ord_size)

    def _apply_mkt_sell(self, ord_size: int) -> None:
        """A recursive function that applies a market sell order."""
        # Base case: order size is fine to be processed
        if self.order_book.loc[self.order_book["NBBO"] == "bid"]["bid size"].values >= ord_size:
            self.order_book.loc[self.order_book["NBBO"] == "bid", "bid size"] -= ord_size
        else:
            # Otherwise move NBBO and apply size
            self._move_nbbo("down")
            self._apply_mkt_sell(ord_size)

    def _move_nbbo(self, direction: str) -> None:
        # Get the index of the best bid
        ind = self.order_book.index[self.order_book["prices"] == self.best_bid]
        if direction == "up":
            self.best_ask = float(self.order_book.iloc[ind-2]["prices"])
            self.best_bid = float(self.order_book.iloc[ind-1]["prices"])
            self.order_book["NBBO"] = self._set_nbbo_col(self.order_book)
        else:
            self.best_bid = float(self.order_book.iloc[ind+1]["prices"])
            self.best_ask = float(self.order_book.iloc[ind]["prices"])
            self.order_book["NBBO"] = self._set_nbbo_col(self.order_book)

    def _generate_order(self) -> tuple:
        """
        Generate a random trading event.
        Returns a tuple with (order type, order side, order size, price of order).
        """
        ord_type = np.random.choice(["MKT", "LMT"], size=1, p=[0.8, 0.2])[0]
        ord_side = np.random.choice(["BUY", "SELL"], size=1, p=[0.5, 0.5])[0]
        ord_size = np.random.normal(loc=100, scale=5, size=1).astype(int)
        if ord_type == "MKT":
            if ord_side == "BUY":
                return ("MKT", ord_side, ord_size, self.best_ask)
            else:
                return ("MKT", ord_side, ord_size, self.best_bid)
        else:
            if ord_side == "BUY":
                price = np.random.choice(np.arange(self._min_price, self.best_ask, 0.01), size=1)[0]
                return ("LMT", ord_side, ord_size, price)
            else:
                price = np.random.choice(np.arange(self.best_bid, self._max_price, 0.01), size=1)[0]
                return ("LMT", ord_side, ord_size, price)


if __name__ == "__main__":
    playbook = RandomStockOrderBook()
    playbook.generate_session()
