class Order:
    """An order.

    Attributes
    ----------
    id_: int
        Integer id of this order.
    client_id: int
        Id of client who put this order in. This is not publicly available
        for most securities.
    """
    id: int
    client_id: int

    def __init__(self, id_: int, client_id: int) -> None:
        """Initialize this order."""
        self.id = id_
        self.client_id = client_id


class MarketOrder(Order):
    """A market order."""
    pass
